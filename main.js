// Assignment 4

//! Char reverse handler

// function charReverseHandler (text) {
//     // your code here
//     let textRender  = text.split('');
//     let renderArr = [];
//     textRender.forEach((elem) => {
//         if (elem == elem.toUpperCase()) {
//             renderArr.push(elem.toLowerCase());
//         }else if(elem == elem.toLowerCase()){
//             renderArr.push(elem.toUpperCase());
//         }
//     })

//     console.log(renderArr.join(''));
// }
// charReverseHandler('The Quick Brown Fox')

//! Todo list

// your code here

// const elForm = document.querySelector("#form");
// const elInput = document.querySelector(".main__input");
// const elList = document.querySelector(".elList");
// const todoArr = [];

// function renderArr(params) {
//     elList.innerHTML = '';
//     params.forEach((elem) => {

//         const elItem = document.createElement("li")
//         elItem.classList.add("list__item")
//         elItem.innerText = elem.text;          
//         elList.append(elItem);
//     })
//     console.log(todoArr)
// }

// elForm.addEventListener("submit", (e) => {
//     e.preventDefault();
//     const elInputValue = elInput.value;
//     todoArr.push({
//         text: elInputValue,
//     })
//     renderArr(todoArr)
//     elInput.value = null;
// })

//! find dublicat handler

// function findDublicatedHandler () {
//     // your code here
//     let arrOne = [1, 3, 4];
//     let arrTwo = [1, 3, 5];
//     let renderArray = arrOne.concat(arrTwo);
//     const filterArray = renderArray.filter((elem, index) => renderArray.indexOf(elem) !== index);
//     console.log(filterArray);
// }

// findDublicatedHandler ();

//! union Handler

// function unionHandler () {
//     // your code here
//     let num_1 = [1, 2, 5]
//     let num_2 = [6, 9, 3]
//     console.log(num_1.concat(num_2).sort((a, b) => a-b));
// }
// unionHandler ()

//? remove boolean handler

// function booleanRemoveHandler (elem) {
//     // your code here
//     let removeBoolen = elem.filter((Boolean));
//     console.log(removeBoolen);
// }

// booleanRemoveHandler ([NaN, false, 32, 'Islom', null, '', undefined])

//? reverse string handler

function reverseStringHandler (param) {
    // your code here
    console.log(param.split('').sort((a, b) => {
        let x = a.toUpperCase();
            y = b.toUpperCase();
        if (x > y)
        return 1;
        if (x < y)
        return -1;
        return 0;
    }).join(''));
}
reverseStringHandler('wEbmASter')